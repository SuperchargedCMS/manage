set -e

url="https://bitbucket.org/SuperchargedCMS/releases/raw/master/latest/release.tar"
dst="$HOME/app"

if [ -e "$dst" ]; then
  echo
  echo -e "\033[0;31mERROR\033[0m: \"$dst\" already exists"
  echo "       To update it use (same url)/update.sh"
  exit 1
fi

mkdir "$dst"
auth="SuperchargedCMS"
[[ "$SCMS_PASS" ]] && auth="$auth:$SCMS_PASS"

tmpfile="/tmp/scms-$USER"
rm -f "$tmpfile"
curl -u "$auth" -o "$tmpfile" "$url"
tar -x -C "$dst" -f "$tmpfile"
rm -f "$tmpfile"

echo
echo -e "\033[0;32mSUCCESS\033[0m: SuperchargedCMS successfully installed into \"$dst\""
echo
