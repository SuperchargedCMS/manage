# set -e  # do not use this here cause we need to revert in case of errors

folders="base docker public/admin/a2 public/admin/ckeditor"

url="https://bitbucket.org/SuperchargedCMS/releases/raw/master/latest/release.tar"
now=$(date "+%Y-%m-%d_%H:%M:%S")
app="$HOME/app"
tmp="$HOME/tmp/$now"
bkp="$HOME/__app_partial_backups__/$now"

function puts {
  echo -e "\033[0;36m$@\033[0m"
}

function run {
  puts ":> $@"
  "$@"
}

function download {
  auth="SuperchargedCMS"
  [[ "$SCMS_PASS" ]] && auth="$auth:$SCMS_PASS"
  tmpfile="/tmp/scms-$USER"
  curl -u "$auth" -o "$tmpfile" "$url" || exit 1
  run tar -x -C "$tmp" -f "$tmpfile"   || exit 1
  run rm  -f "$tmpfile"
}

function backup {
  puts "Performing backups..."
  run mkdir -p "$bkp" || exit 1
  run rsync       \
    -a             \
    --exclude db    \
    --exclude public \
    "$app/" "$bkp" || exit 1
}

function revert {
  puts "Something went awry, reverting..."
  run rsync -av "$bkp/" "$app" || exit 1
}

function revert_and_exit {
  cleanup
  revert
  exit 1
}

function update {
  puts "Updating..."
  run rsync      \
    -a            \
    --delete       \
    --exclude db    \
    --exclude db.sql \
    --exclude public  \
    --exclude config   \
    --exclude admin.htpasswd \
    --exclude custom_controllers \
    "$tmp/" "$app" || revert_and_exit
  run rsync \
    -a       \
    --delete  \
    "$tmp/public/admin" "$app/public" || revert_and_exit
}

function cleanup {
  puts "Cleaning up..."
  run rm -fr "$tmp"
}

echo
puts "~~~~~~~~~~~~~~~ $USER ~~~~~~~~~~~~~~~"

rm    -fr "$tmp" || exit 1
mkdir -p  "$tmp" || exit 1
rm    -fr "$bkp" || exit 1
mkdir -p  "$bkp" || exit 1
mkdir -p  "$app" || exit 1

download
backup
update
cleanup

puts "============ SUCCESS: $USER successfully updated ============"
